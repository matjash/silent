#!/bin/bash
echo "Scripts installs apache2 and copyies config from git directory."

# FQDN as arg1
if [[ $1 = "" ]]; then return fi

# Install apache 2
sudo apt-get update -y && apt-get install apache2 -y  

# Copy clean configs
#sudo curl https://gitlab.com/matjash/silent/apache2/etc/apache2/apache2.conf > /etc/apache2/apache2.conf
#sudo curl https://gitlab.com/matjash/silent/apache2/etc/apache2/sites-available/000-default.conf > /etc/apache2/sites-available/000-default.conf
#sudo curl https://gitlab.com/matjash/silent/apache2/etc/apache2/sites-available/default-ssl.conf > /etc/apache2/sites-available/default-ssl.conf

# Set server name (instead www.example.com)
#sudo sed -i "s,www.example.com,${FQDN}i,g" /etc/apache2/apache2.conf
#&& sed -i "s,www.example.com,${FQDN}i,g" /etc/apache2/sites-available/000-default.conf
#&& sed -i "s,www.example.com,${FQDN}i,g" /etc/apache2/sites-available/default-ssl.conf

# Enable modules
#sudo a2ensite default-ssl.conf
#&& a2enmod ssl
#&& service apache2 restart
